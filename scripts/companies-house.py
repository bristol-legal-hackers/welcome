import requests
import base64
from pprint import pprint

# pylint:disable=all

token = 'YOUR API TOKEN GOES HERE'

def get(query):
    encoded = base64.b64encode(token.encode()).decode()

    header = {
        'Authorization': 'Basic '+encoded
    }

    params = {
        'q': query
    }

    return requests.get(
        'https://api.companieshouse.gov.uk/search/companies', params=params, headers=header).json()


pprint(get('grow bristol'))
