import datetime
import requests

ipinfo = requests.get('https://ipinfo.io').json()

city = ipinfo['city']
lat, lon = ipinfo['loc'].split(',')

iss_data = requests.get('http://api.open-notify.org/iss-pass.json?lat={}&lon={}'.format(lat, lon)).json()

time = datetime.datetime.utcfromtimestamp(iss_data['response'][0]['risetime'])

print('ISS next over {city} at {time}'.format(city=city, time=time))
