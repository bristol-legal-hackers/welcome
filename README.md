# Welcome

### What?
to provide introduction an introduction to coding and computing principles.
we will be using Python3 & Jupyter to intergrate with outher software such as APIs, Excel, Databases and Word Proccessing.
the seccions, have a very casual format, but (TBC) will be 2 or 3hrs. with ~30min arrivle time ~40min talk/demo on a priciple or feture then 1+hrs practice . 

### When?
Wednesday every fourtnight.  
Next Lesson 11th March.  
events can be found at [eventbrite](https://www.eventbrite.co.uk/o/marc-may-and-ed-le-gassick-25710880085)

### Where?
RPC, Bristol.

### You Will Need
yourself...  
a laptop that you can install programs on.  
you will have to install python and jupyter on you laptop to partake in the lesson. if you can do this before arriving this will save a lot of time.  

### Installing Python on Windows
https://docs.python.org/3/using/windows.html

### Installing Python on Apple
https://docs.python.org/3/using/mac.html

### Instaling python on Linux
your running Linux!? you dont need my help ;)

## Installing Juputer Notebook
after installing python you will need to install Jupyter Notebook  
https://jupyter.org/

# Lessong 1 
## Veriables, Data-types and Operators



