# Data Types

To find out a veriables type use the type function.  
`type(value)`

## Core Data Types

### Boolean
True of False.  
`value = True`

### Integer 
a whole number.  
`value = 7`

### Float 
a decimal point number.  
`value = 7.5`

### String
plain text e.g. letters and/or numbers.  
`value = "Seven"` must be in quotes.

## Basic Collection Data Types

### List
a list of veriables.  
`value = []` or `value = list()`  these create empty lists.  
`value = [1,True, "hello",7.5]`  creates a list containing four things.  
`value = list([1,True, "hello",7.5])`

### Dictionary 
a named list of veriables.  
`value = {}` or `value = dict()`
